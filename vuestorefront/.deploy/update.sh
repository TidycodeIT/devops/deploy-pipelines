echo "STARTED GIT PULL SCRIPT"

git reset --hard
git clean -df
git checkout $1
git pull

echo "ENDED GIT PULL SCRIPT"