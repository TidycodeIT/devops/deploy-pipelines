#!/bin/bash

echo $(whoami)
echo $(id)

folder_to_remove="generated"
max_attempts=5
attempts=0

cd $1

echo "STARTED PRE DEPLOY SCRIPT"

warden env exec -T php-fpm php bin/magento maintenance:enable

while [ $attempts -lt $max_attempts ]; do
    rm -r "$folder_to_remove"

    if [ $? -eq 0 ]; then
        echo "Folder $folder_to_remove removed successfully."
        break
    else
        echo "Error removing the folder. Attempt $((attempts + 1)) of $max_attempts."
        let "attempts++"
        sleep 1  # Wait for one second before retrying
    fi
done

if [ $attempts -eq $max_attempts ]; then
    echo "Unable to remove the folder after $max_attempts attempts. Check permissions or other issues."
fi

echo "STARTED POST DEPLOY SCRIPT"

echo "STARTED DEPLOY SCRIPT"

warden env exec -T php-fpm php tools/composer install
warden env exec -T php-fpm php bin/magento deploy:mode:set production -s
warden env exec -T php-fpm php bin/magento s:d:c
warden env exec -T php-fpm php bin/magento s:up --keep-generated
rm -rf pub/static/* var/view_preprocessed/*
warden env exec -T php-fpm php bin/magento s:s:d -j 10

echo "ENDED POST DEPLOY SCRIPT"

echo "STARTED POST DEPLOY SCRIPT"

warden env exec -T php-fpm php bin/magento maintenance:disable
warden env exec -T php-fpm php bin/magento c:f

echo "ENDED POST DEPLOY SCRIPT"
