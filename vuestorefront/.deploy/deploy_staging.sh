#!/bin/bash

echo "STARTED DEPLOY SCRIPT - STOREFRONT"

cd vue-storefront
yarn 
yarn build 
pm2 delete server 
yarn start 
cd $1

echo "ENDED DEPLOY SCRIPT - STOREFRONT"

echo "STARTED DEPLOY SCRIPT - API"

cd vue-storefront-api
yarn 
yarn build 
pm2 delete o2m api
yarn start 
cd $1

echo "ENDED DEPLOY SCRIPT - API"

redis-cli flushall

echo "ENDED POST DEPLOY SCRIPT"
