#!/bin/bash

folder_to_remove="generated"
max_attempts=5
attempts=0

cd $1

echo "STARTED PRE DEPLOY SCRIPT"

mv .user.ini .user.ini_opcache
mv .user.ini_no_opcache .user.ini
mv pub/.user.ini pub/.user.ini_opcache
mv pub/.user.ini_no_opcache pub/.user.ini

php bin/magento maintenance:enable

while [ $attempts -lt $max_attempts ]; do
    rm -r "$folder_to_remove"

    if [ $? -eq 0 ]; then
        echo "Folder $folder_to_remove removed successfully."
        break
    else
        echo "Error removing the folder. Attempt $((attempts + 1)) of $max_attempts."
        let "attempts++"
        sleep 1  # Wait for one second before retrying
    fi
done

if [ $attempts -eq $max_attempts ]; then
    echo "Unable to remove the folder after $max_attempts attempts. Check permissions or other issues."
fi

echo "STARTED POST DEPLOY SCRIPT"

echo "STARTED DEPLOY SCRIPT"

php tools/composer install
php bin/magento deploy:mode:set production -s
php bin/magento s:d:c
php bin/magento s:up --keep-generated
rm -rf pub/static/* var/view_preprocessed/*
php bin/magento s:s:d -j 10 -f -t Magento/backend -t Freeshop/checkout -t Freeshop/hyva -t Tidycode/checkout --language=it_IT --language=en_US

echo "ENDED POST DEPLOY SCRIPT"

echo "STARTED POST DEPLOY SCRIPT"

php bin/magento maintenance:disable
php bin/magento c:f

mv .user.ini .user.ini_no_opcache
mv .user.ini_opcache .user.ini
mv pub/.user.ini pub/.user.ini_no_opcache
mv pub/.user.ini_opcache pub/.user.ini

echo -e "\n\n\e[1;43m Se sei nell'ambiente di produzione ricordati di pulire la cache di Apache dal pannellino di Ittweb\e[0m\n"

echo "ENDED POST DEPLOY SCRIPT"
